namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProductLink : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SearchSpecs", "ProductLinkSelector", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SearchSpecs", "ProductLinkSelector");
        }
    }
}
