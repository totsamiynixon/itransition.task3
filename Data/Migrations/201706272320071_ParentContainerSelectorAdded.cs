namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ParentContainerSelectorAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SearchSpecs", "ParentContainerSelector", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SearchSpecs", "ParentContainerSelector");
        }
    }
}
