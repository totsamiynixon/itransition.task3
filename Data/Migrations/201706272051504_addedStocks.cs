namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedStocks : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SearchSpecs", "StockSelector", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SearchSpecs", "StockSelector");
        }
    }
}
