﻿using Core.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Data.Context;

namespace Data.Repositories
{
    public class EntityRepository<TEntity> : IRepository<TEntity> where TEntity : class, IDataEntity
    {
        private readonly DataContext _context;
        private readonly IDbSet<TEntity> _dbEntitySet;
       

        public EntityRepository(DataContext context)
        {
            _context = context;
            _dbEntitySet = _context.Set<TEntity>();
        }

        public void Insert(TEntity entity)
        {
            _dbEntitySet.Add(entity);
        }

        public void Update(TEntity entity)
        {
            var dbEntityEntry = _context.Entry<TEntity>(entity);
            if (dbEntityEntry.State == EntityState.Detached)
            {
                var dbSet = _context.Set<TEntity>();
                var entityInContext = dbSet.Local.FirstOrDefault(f => f.Id == entity.Id);
                if (entityInContext != null)
                {
                    var attachedEntry = _context.Entry(entityInContext);
                    attachedEntry.CurrentValues.SetValues(entity);
                }
                else
                {
                    dbSet.Attach(entity);
                    dbEntityEntry.State = EntityState.Modified;
                }
            }
            else
            {
                dbEntityEntry.State = EntityState.Modified;
            }
          
        }

        public void Delete(TEntity entity)
        {
            _dbEntitySet.Remove(entity);
        }

      

        public IQueryable<TEntity> Collection => _dbEntitySet.AsNoTracking();

        public IQueryable<TEntity> CollectionWithTracking => _dbEntitySet;



        public async Task<List<TEntity>> GetAllAsync(
            Expression<Func<TEntity, bool>> predicate,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy,
            params Expression<Func<TEntity, object>>[] includeProperties)
        {
            var entities = FilterQuery(orderBy, predicate, includeProperties);
            var list = await entities.ToListAsync();
            return list;
        }

    

        public async Task<TCustom> GetCustomAsync<TCustom>(
            Expression<Func<TEntity, bool>> predicate,
            Func<IQueryable<TEntity>, Task<TCustom>> buildCustomFunc,
            params Expression<Func<TEntity, object>>[] includeProperties)
        {
            var entities = FilterQuery(null, predicate, includeProperties);
            return await buildCustomFunc(entities);
        }

        private IQueryable<TEntity> FilterQuery(
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy,
            Expression<Func<TEntity, bool>> predicate,
            Expression<Func<TEntity, object>>[] includeProperties)
        {
            var entities = IncludeProperties(includeProperties);
            entities = (predicate != null) ? entities.Where(predicate) : entities;
            if (orderBy != null)
            {
                entities = orderBy(entities);
            }
            return entities;
        }


        private IQueryable<TEntity> IncludeProperties(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            IQueryable<TEntity> entities = _dbEntitySet
                    .AsNoTracking()
                ;
            foreach (var includeProperty in includeProperties)
            {
                entities = entities.Include(includeProperty);
            }
            return entities;
        }
    }
}

