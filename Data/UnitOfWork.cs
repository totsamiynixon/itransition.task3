﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Core.Data;
using Data.Context;
using Data.Repositories;

namespace Data
{
    public class UnitOfWork : IUnitOfWork
    {

        private readonly DataContext _context;
        private bool _disposed;
        private Hashtable _repositories;

        public UnitOfWork(DataContext context)
        {
            _context = context;
        }

        public IRepository<TEntity> Repository<TEntity>()
            where TEntity : class, IDataEntity
        {
            if (_repositories == null)
            {
                _repositories = new Hashtable();
            }

            var type = typeof(TEntity);
            var typeName = type.Name;
            if (!_repositories.ContainsKey(typeName))
            {
                var repositoryType = typeof(EntityRepository<>);
                _repositories.Add(typeName, Activator.CreateInstance(repositoryType.MakeGenericType(typeof(TEntity)), _context));
            }
            return (IRepository<TEntity>)_repositories[typeName];
        }

        public int SaveChanges()
        {
            try
            {
                return _context.SaveChanges();
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public async Task<int> SaveChangesAsync()
        {
           
                return await _context.SaveChangesAsync();

        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            
                return await _context.SaveChangesAsync(cancellationToken);
            
        }

        public void Dispose()
        {
            //Dispose(true);
            //GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            //if (!_disposed && disposing)
            //{
            //    _repositories = null;

            //    _context.Dispose();
            //}
            //_disposed = true;
        }
    }
}
