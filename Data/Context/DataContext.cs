﻿using Core.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Context
{
    public  class DataContext : DbContext
    {

        public DataContext(): base ("DefaultConnection")
        {
            
        }
        public virtual DbSet<SearchSpec> SearchSpecs { get; set; }
    }
}
