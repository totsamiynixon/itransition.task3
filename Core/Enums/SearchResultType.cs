﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Enums
{
    public enum SearchResultType
    {
        HTML = 1,
        JSON = 2
    }
}
