﻿using Core.Data;
using Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain
{
    public class SearchSpec : IDataEntity
    {
        public int Id { get; set; }
        
        public string SearchUrl { get; set; }

        public string ParentContainerSelector { get; set; }

        public string NameSelector { get; set; }

        public string PriceSelector { get; set; }

        public string StockSelector { get; set; }

        public string ImageSelector { get; set; }

        public string ProductLinkSelector { get; set; }

        public SearchResultType SearchResultType { get; set; }

        public bool Active { get; set; }

    }
}
