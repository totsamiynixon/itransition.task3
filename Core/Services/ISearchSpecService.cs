﻿using Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Services
{
    public interface ISearchSpecService
    {
        Task<int> AddOrUpdate(SearchSpec searchSpec);

        Task Remove(int id);

        Task Remove(List<int> ids);

        Task<List<SearchSpec>> GetAll(bool withDeactivated = false);

        Task<SearchSpec> GetById(int id);

        Task<SearchSpec> GetByUrl(string url);
    }
}
