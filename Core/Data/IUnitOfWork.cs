﻿using Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Core.Data
{
    public interface IUnitOfWork
    {
        void Dispose(bool disposing);

        IRepository<TEntity> Repository<TEntity>() where TEntity : class, IDataEntity;

        int SaveChanges();

        Task<int> SaveChangesAsync();

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
