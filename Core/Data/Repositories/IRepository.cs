﻿using Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface IRepository<TEntity> where TEntity: class, IDataEntity
    {
        void Insert(TEntity entity);

        void Update(TEntity entity);

        void Delete(TEntity entity);


        IQueryable<TEntity> Collection { get; }

        IQueryable<TEntity> CollectionWithTracking { get; }

        Task<List<TEntity>> GetAllAsync(
            Expression<Func<TEntity, bool>> predicate,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy,
            params Expression<Func<TEntity, object>>[] includeProperties);

        Task<TCustom> GetCustomAsync<TCustom>(
            Expression<Func<TEntity, bool>> predicate,
            Func<IQueryable<TEntity>, Task<TCustom>> buildCustomFunc,
            params Expression<Func<TEntity, object>>[] includeProperties);
    }
}

