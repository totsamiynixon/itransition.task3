﻿using Core.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Core.Data;
using Core.Domain;
using Data.Repositories;

namespace Services
{
    public class SearchSpecService: ISearchSpecService
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<SearchSpec> _searchSpecRepository;

        public SearchSpecService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _searchSpecRepository = unitOfWork.Repository<SearchSpec>();

        }
        public async Task<int> AddOrUpdate(SearchSpec searchSpec)
        {
            if (searchSpec == null)
                return 0;

            SearchSpec searchSpecInDb = null;

            if (searchSpec.Id > 0)
            {
                searchSpecInDb = await _searchSpecRepository
                    .CollectionWithTracking
                    .FirstOrDefaultAsync(f => f.Id == searchSpec.Id);
            }

            if (searchSpecInDb == null)
            {
                _searchSpecRepository.Insert(searchSpec);
                await _unitOfWork.SaveChangesAsync();
                return searchSpec.Id;
            }

            Mapper.Map(searchSpec, searchSpecInDb);
            _searchSpecRepository.Update(searchSpecInDb);
            await _unitOfWork.SaveChangesAsync();
            return searchSpecInDb.Id;
        }

        public Task Remove(int ide)
        {
            throw new System.NotImplementedException();
        }

        public Task Remove(List<int> ids)
        {
            throw new System.NotImplementedException();
        }

        public Task<List<SearchSpec>> GetAll(bool withDeactivated = false)
        {
            if (withDeactivated)
            {
                return _searchSpecRepository.Collection.ToListAsync();
            }
            else
            {
                return  _searchSpecRepository.Collection.Where(ss => ss.Active).ToListAsync();
            }
        }
       

        public Task<SearchSpec> GetById(int id)
        {
            return _searchSpecRepository.Collection.FirstOrDefaultAsync(s => s.Id == id);
        }

        public Task<SearchSpec> GetByUrl(string url)
        {
          
            return _searchSpecRepository.Collection.FirstOrDefaultAsync(s => s.SearchUrl == url);
        }
    }
}
