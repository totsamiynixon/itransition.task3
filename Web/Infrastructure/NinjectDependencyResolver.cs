﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.Data;
using Core.Domain;
using Core.Services;
using Data;
using Data.Context;
using Data.Repositories;
using Ninject;
using Ninject.Web.Common;
using Services;
using Web.Helpers;
using Web.Infrastructure.Helpers;

namespace Web.Infrastructure
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;
        public NinjectDependencyResolver(IKernel kernelParam)
        {
            kernel = kernelParam;
            AddBindings();
        }
        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }
        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }
        private void AddBindings()
        {
            kernel.Bind<IUnitOfWork>().To<UnitOfWork>().InRequestScope();
            kernel.Bind<ISearchSpecService>().To<SearchSpecService>().InRequestScope();
            kernel.Bind<DataContext>().To<DataContext>().InRequestScope();
            kernel.Bind<IHttpHelper>().To<HttpHelper>().InRequestScope();
        }
    }
}