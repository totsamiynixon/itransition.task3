﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Domain;
using Web.Models.SearchSpec;

namespace Web.Infrastructure.Helpers
{
    public interface IHttpHelper
    {
        Task<string> MakeQuery(string queryPath, string queryArg);

        List<SearchItem> GetItemsFromHtml(string html, SearchSpec searchSpec);

        List<SearchItem> GetItemsFromJson(string jsonString, SearchSpec searchSpec);
    }
}
