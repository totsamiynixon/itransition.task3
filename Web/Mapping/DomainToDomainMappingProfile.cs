﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Core.Domain;

namespace Web.Mapping
{
    public class DomainToDomainMappingProfile : Profile
    {
        public DomainToDomainMappingProfile()
        {
            CreateMap<SearchSpec, SearchSpec>()
                .ForMember(f => f.Id, m => m.Ignore());

        }
    }
}