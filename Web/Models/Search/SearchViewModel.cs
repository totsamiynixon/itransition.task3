﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Models.Search
{
    public class SearchViewModel
    {
        public SelectList SearchUrls { get; set; }
        
        public List<string> SelectedUrls { get; set; }
       
        public string QueryString { get; set; }

    }
}