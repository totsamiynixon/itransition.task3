﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models.SearchSpec;

namespace Web.Models
{
    public class SearchResult
    {
        public string Domain { get; set; }

        public List<SearchItem> SearchItems { get; set; }
    }
}