﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models.SearchSpec
{
    public class SearchSpecTestViewModel
    {
        public string SearchQuery { get; set; }
        public int SearchSpecId { get; set; }
        public List<SearchItem> SearchItems { get; set; }
    }
}