﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models.SearchSpec
{
    public class SearchItem
    {
        public string Name { get; set; }
        public string Price { get; set; }
        public string Stock { get; set; }
        public string ProductLink { get; set; }
        public string ImageUrl { get; set; }

    }
}