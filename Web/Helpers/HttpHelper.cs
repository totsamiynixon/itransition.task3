﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using Core.Domain;
using CsQuery;
using Newtonsoft.Json.Linq;
using Web.Infrastructure.Helpers;
using Web.Models.SearchSpec;

namespace Web.Helpers
{
    public class HttpHelper : IHttpHelper
    {

        public  async Task<string> MakeQuery(string queryPath, string queryArg)
        {
            using (var client = new HttpClient())
            {
                var uri = new Uri(queryPath + queryArg);
                client.DefaultRequestHeaders.Accept.Add(MediaTypeWithQualityHeaderValue.Parse("application/json"));
                var response = await client.GetAsync(uri);
                return await response.Content.ReadAsStringAsync();
            }
        }

        public List<SearchItem> GetItemsFromHtml(string html, SearchSpec searchSpec)
        {
            CQ cq = CQ.CreateDocument(html);
            var listItems = new List<SearchItem>();
            var parents = cq.Find(searchSpec.ParentContainerSelector);
            foreach (var parent in parents)
            {
                var price = parent.Cq().Find(searchSpec.PriceSelector).Select(s => s.InnerHTML).FirstOrDefault();
                var name = parent.Cq().Find(searchSpec.NameSelector).Select(s => s.InnerHTML).FirstOrDefault();
                var stock = String.Join(", ",parent.Cq().Find(searchSpec.StockSelector).Select(s => s.InnerHTML));
                var imageUrl = parent.Cq().Find(searchSpec.ImageSelector).Select(s => s.GetAttribute("src")).FirstOrDefault();
                var productLink = parent.Cq().Find(searchSpec.ProductLinkSelector).Select(s => s.GetAttribute("href")).FirstOrDefault();
                listItems.Add(new SearchItem
                {
                    ImageUrl = ValidateUrl(imageUrl, searchSpec.SearchUrl),
                    Name = HttpUtility.HtmlDecode(RemoveTags(name)),
                    Price = HttpUtility.HtmlDecode(RemoveTags(price)),
                    Stock = HttpUtility.HtmlDecode(RemoveTags(stock)),
                    ProductLink = ValidateUrl(productLink,searchSpec.SearchUrl)
                });
            }
            return listItems.ToList();
        }

        public List<SearchItem> GetItemsFromJson(string jsonString, SearchSpec searchSpec)
        {
            var prefix = "$..";
            var jsonResult = JToken.Parse(jsonString);
            var parents = jsonResult.SelectTokens(prefix + searchSpec.ParentContainerSelector).Children();
            var listItems = new List<SearchItem>();
            foreach (var parent in parents.ToList())
            {
                var priceToken = parent.SelectToken(searchSpec.PriceSelector);
                var nameToken = parent.SelectToken(searchSpec.NameSelector);
                var stockToken = parent.SelectToken(searchSpec.StockSelector);
                var imageUrlToken = parent.SelectToken(searchSpec.ImageSelector);
                var linkToken = parent.SelectToken(searchSpec.ProductLinkSelector);
                listItems.Add(new SearchItem
                {
                    Name = nameToken?.ToString(),
                    Price = priceToken?.ToString(),
                    ImageUrl = imageUrlToken?.ToString(),
                    Stock = stockToken?.ToString(),
                    ProductLink = linkToken?.ToString()
                   
                });
            }
            return listItems;
        }

        private string RemoveTags(string text)
        {
            return Regex.Replace(text, "<[^>]+>", string.Empty);
        }

        private string ValidateUrl(string text, string domain)
        {
            if (text == null)
            {
                return null;
            }
            if (text.StartsWith("http"))
            {
                return text;
            }
            var uri = new Uri(domain);
            string host = uri.DnsSafeHost;
            return text.Insert(0, uri.AbsoluteUri.Replace(uri.PathAndQuery, "/"));
        }

    }
}
