﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Core.Domain;
using Core.Enums;
using Core.Services;
using Web.Infrastructure.Helpers;
using Web.Models;
using Web.Models.Search;
using Web.Models.SearchSpec;

namespace Web.Controllers
{
    public class SearchController : Controller
    {
        private readonly IHttpHelper _httpHelper;
        private readonly ISearchSpecService _searchSpecService;

        public SearchController(IHttpHelper httpHelper, ISearchSpecService searchSpecService)
        {
            _httpHelper = httpHelper;
            _searchSpecService = searchSpecService;
        }
        // GET: Search
        public async Task<ActionResult> Index()
        {
            var searchSpecifications = await _searchSpecService.GetAll();
            var model = new SearchViewModel
            {
                SearchUrls = new SelectList(searchSpecifications.Select(s => s.SearchUrl))
            };
            return View(model);
        }

        public async Task<ActionResult> Search(SearchViewModel model)
        {
         
            List<SearchSpec> searchSpecifications = new List<SearchSpec>();
            foreach (var url in model.SelectedUrls)
            {
                searchSpecifications.Add(await _searchSpecService.GetByUrl(url));
            }

            List<SearchResult> searchResults = new List<SearchResult>();
            foreach (var searchSpecification in searchSpecifications)
            {
                searchResults.Add(new SearchResult
                {
                    SearchItems = await GetSearchItemsByQuery(model.QueryString, searchSpecification),
                    Domain = new Uri(searchSpecification.SearchUrl).Host
                });

            }
            return PartialView(searchResults);

        }

        private async Task<List<SearchItem>> GetSearchItemsByQuery(string query, SearchSpec searchSpec)
        {

            List<SearchItem> searchItems = new List<SearchItem>();
            var queryResult = await _httpHelper.MakeQuery(searchSpec.SearchUrl, query);
            queryResult = HttpUtility.HtmlDecode(queryResult);
            switch (searchSpec.SearchResultType)
            {
                case SearchResultType.HTML:
                    {

                        searchItems = _httpHelper.GetItemsFromHtml(queryResult, searchSpec);
                        break;
                    }
                case SearchResultType.JSON:
                    {
                        searchItems = _httpHelper.GetItemsFromJson(queryResult, searchSpec);
                        break;
                    }
            }
            return searchItems;
        }
    }
}