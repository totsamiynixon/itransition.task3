﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Core.Domain;
using Core.Enums;
using Core.Services;
using CsQuery;
using CsQuery.ExtensionMethods.Internal;
using Data.Context;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Web.Infrastructure.Helpers;
using Web.Models.SearchSpec;
using WebGrease.Css.Extensions;

namespace Web.Controllers
{
    public class SearchSpecsController : Controller
    {
        private readonly ISearchSpecService _searchSpecService;
        //There was two different ways to realize functionality of HttpHelper
        //The first one is create helper like i did
        //The second one is create base constroller whith this functionality and use inheritance
        private readonly IHttpHelper _httpHelper;

        public SearchSpecsController(ISearchSpecService searchSpecService, IHttpHelper httpHelper)
        {
            _searchSpecService = searchSpecService;
            _httpHelper = httpHelper;
        }

        // GET: SearchSpecs
        public async Task<ActionResult> Index()
        {
            return View(await _searchSpecService.GetAll());
        }

        // GET: SearchSpecs/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SearchSpec searchSpec = await _searchSpecService.GetById(id.Value);
            if (searchSpec == null)
            {
                return HttpNotFound();
            }
            return View(searchSpec);
        }

        // GET: SearchSpecs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SearchSpecs/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(
            SearchSpec searchSpec)
        {
            if (ModelState.IsValid)
            {
                await _searchSpecService.AddOrUpdate(searchSpec);
                return RedirectToAction("Index");
            }

            return View(searchSpec);
        }

        // GET: SearchSpecs/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SearchSpec searchSpec = await _searchSpecService.GetById(id.Value);
            if (searchSpec == null)
            {
                return HttpNotFound();
            }
            return View(searchSpec);
        }

        // POST: SearchSpecs/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(
            
            SearchSpec searchSpec)
        {
            if (ModelState.IsValid)
            {
                await _searchSpecService.AddOrUpdate(searchSpec);
                return RedirectToAction("Index");
            }
            return View(searchSpec);
        }

        // GET: SearchSpecs/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SearchSpec searchSpec = await _searchSpecService.GetById(id.Value);
            if (searchSpec == null)
            {
                return HttpNotFound();
            }
            return View(searchSpec);
        }

        // POST: SearchSpecs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {

            await _searchSpecService.Remove(id);
            return RedirectToAction("Index");
        }

#if DEBUG

        [HttpGet]
        public ViewResult Test(int id)
        {
            var searchSpecTestVM = new SearchSpecTestViewModel
            {
                SearchSpecId = id
            };
            return View(searchSpecTestVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Test(SearchSpecTestViewModel model)
        {
            var searchSpec = await _searchSpecService.GetById(model.SearchSpecId);
            var queryResult = await _httpHelper.MakeQuery(searchSpec.SearchUrl, model.SearchQuery);
            queryResult = HttpUtility.HtmlDecode(queryResult);
            switch (searchSpec.SearchResultType)
            {
                case SearchResultType.HTML:
                {

                    model.SearchItems = _httpHelper.GetItemsFromHtml(queryResult, searchSpec);
                    break;
                }
                case SearchResultType.JSON:
                {
                    model.SearchItems = _httpHelper.GetItemsFromJson(queryResult, searchSpec);
                    break;
                }
            }
            return View(model);
        }

#endif
    }
}
